Google Tag Manager
------------------

Overview
--------

This module provides a simple method of configuring Google Tag Manager[1] for 
use within Drupal.

Installation
------------

Unpack the module archive under 'sites/all/modules' and enable it via 
Administration -> Modules

[1] http://www.google.com/tagmanager
