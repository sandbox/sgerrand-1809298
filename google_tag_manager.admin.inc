<?php

/**
 * @file google_tag_manager.admin.inc
 * Covers administrative interface hook functions.
 * @package google_tag_manager
 */

/**
 * Implements hook_admin_settings().
 */
function google_tag_manager_admin_settings_form($form_state) {
  $form['account'] = array(
    '#title' => t('General settings'),
    '#type' => 'fieldset',
  );

  $form['account']['google_tag_manager_account'] = array(
    '#default_value' => variable_get('google_tag_manager_account', 'GTM-'),
    '#description' => t('This ID is unique to each account.'),
    '#maxlength' => 20,
    '#required' => TRUE,
    '#size' => 15,
    '#title' => t('Container ID'),
    '#type' => 'textfield',
  );

  return system_settings_form($form);
}

/**
 * Implements hook_form_validate().
 */
function google_tag_manager_admin_settings_form_validate($form, &$form_state) {
  // First, clean the input variables.
  $form_state['values']['google_tag_manager_account'] = trim($form_state['values']['google_tag_manager_account']);

  if (!preg_match('/^GTM-\w+$/', $form_state['values']['google_tag_manager_account'])) {
    form_set_error('google_tag_manager_account',
                   t('A valid Google Tag Manager container ID is case sensitive and is formatted like GTM-XXXX'));
  }
}
